$(document).ready(() => {
    $('#loginButton').click(async (event) => {
        event.preventDefault();
        await sendLoginRequest();
        location.href = 'projects';
    });
    $('#regButton').click((event) => {
        event.preventDefault();
        location.href = 'register';
    });
    $('#regAcceptButton').click((event) => {
        event.preventDefault();
        sendRegisterRequest();
    });
    $('#reg_back_to_login').click((event) => {
        event.preventDefault();
        location.href = '../login.html';
    });
});

let renderError = (msg) => {
    $('#errElement').remove();
    $('#infoElement').remove();
    let errElement  = document.createElement('div');
    errElement.id = 'errElement';
    errElement.textContent = msg;
    errElement.style.color = 'red';
    document.body.appendChild(errElement);
}

let renderInfo = (msg) => {
    $('#infoElement').remove();
    $('#errElement').remove();
    let infoElement  = document.createElement('div');
    infoElement.id = 'infoElement';
    infoElement.textContent = msg;
    infoElement.style.color = 'green';
    document.body.appendChild(infoElement);
}

let convertMiddleware = async (resp) => {
    try {
        var body = await resp.json();
    }catch (e) {
        renderError(`cannot read response body: ${e.message}`);
        return Promise.reject(null);
    }
    if (!resp.ok) {
        renderError(body.error.message);
        return Promise.reject(null);
    }
    return body;
}

let extractToken = async (body) => {
    if (!body.token) {
        return Promise.reject('no token found in response')
    }
    window.sessionStorage.setItem('jwt', body.token);
    return Promise.resolve(null);
}

let sendLoginRequest = () => {
    return fetch("http://vcs-server/auth/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: $('#emailBox').val(),
            password: $('#passwordBox').val(),
        })
    }).then(convertMiddleware)
        .then(extractToken)
        .catch(errMsg => renderError(errMsg));
}

let sendRegisterRequest = () => {
    fetch("http://vcs-server/auth/register", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: $('#regEmailBox').val(),
            password: $('#regPasswordBox').val(),
        })
    }).then(convertMiddleware)
        .then((res) => renderInfo(`successfully registered user ${res.user.email} with id ${res.user.id}`));
}