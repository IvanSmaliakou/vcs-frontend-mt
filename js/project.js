$(document).ready(() => {
    renderProjectPage().then((dt) => {
        let projName = window.localStorage.getItem('clickedProject');
        deleteFileButtonHandler(projName);
        editFileButtonHandler(projName);
        addNewRow(projName, dt);
        editProjectNameHandler();
        editOwnersHandler(projName);
    });
});

let renderProjectPage = async () => {
    let projName = window.localStorage.getItem('clickedProject');
    let body = fetch(`http://vcs-server/v1/project/${projName}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
        },
    }).then(convertMiddleware);
    let project = (await body).project;
    $('#projectNameSpan').text(projName);
    $('#projectOwnersSpan').text(project.owners.join(','));
    let datatable = $('#filesTable').DataTable({
        paging: false,
        info: false,
        searching: false,
        ordering: false,
        language: {
            emptyTable: ' '
        },
        columns: [
            {width: "50%", targets: 0},
            {width: "30%", targets: 0},
            {width: "30%", targets: 0},
            {
                defaultContent: '<button type="submit" class="deleteFileButton">Удалить</button>',
                width: "30%",
                targets: 0
            },
            {
                defaultContent: '<button type="submit" class="editFileButton">Редактировать</button>',
                width: "30%",
                targets: 0
            }
        ],
    });
    project.files.map(file => {
        datatable.row.add([file.name, file.createdAt,
            file.updatedAt]).draw();
    })
    return datatable;
}

let registerAcceptChangesHandler = (projName, filename) => {
    $('#applyFileChangesButton').click(async function (event) {
        event.preventDefault();
        let row = $(this).closest('tr');
        let cells = row.find('td')
            .not('td:nth-child(4)')
            .not('td:nth-child(5)');
        let patch = {};
        cells.each(function (i) {
            if (i === 0) {
                patch.name = $(this).find("input").val();
                $(this).html(patch.name);
                return;
            }
            if (i === 1) {
                patch.createdAt = $(this).find("input").val();
                $(this).html(patch.createdAt);
                return;
            }
            if (i === 2) {
                patch.updatedAt = $(this).find("input").val();
                $(this).html(patch.updatedAt);
            }
        });

        fetch(`http://vcs-server/v1/${projName}/file`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
            },
            body: JSON.stringify({
                name: filename,
                patch: patch,
            })
        })
            .then(convertMiddleware).then(() => renderInfo(`file has been updated`));
        event.target.parentElement.innerHTML = '<button type="submit" class="editFileButton">Редактировать</button>';
        editFileButtonHandler(projName);
        deleteFileButtonHandler(projName);
    });
}

let deleteFileButtonHandler = (projName) => {
    $('.deleteFileButton').click(async event => {
        event.preventDefault();
        var dt = $('#filesTable').DataTable();
        fetch(`http://vcs-server/v1/${projName}/files`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
            },
            body: JSON.stringify({
                files: [dt.row(event.target.parentElement.parentElement).data()[0]]
            })
        }).then(convertMiddleware)
            .then(() => renderInfo(`file has been deleted`));
        dt.row(event.target.parentElement.parentElement).remove();
        dt.draw();
    });
}

let editFileButtonHandler = (projName) => {
    $('.editFileButton').click(async function (event) {
        event.preventDefault();
        let filename;
        let row = $(this).closest('tr');
        let cells = row.find('td')
            .not('td:nth-child(4)')
            .not('td:nth-child(5)');
        cells.each(function (i) {
            let txt = $(this).text();
            if (i === 0) {
                filename = txt;
            }
            $(this).html("").append("<input type='text' value=\"" + txt + "\">");
        })
        event.target.parentElement.innerHTML = '<button type="submit" id="applyFileChangesButton">Применить</button>';
        registerAcceptChangesHandler(projName, filename);
    });
}

let addNewRow = (projName, datatable) => {
    $('#addFileButton').click(function (event) {
        event.preventDefault();
        $("#newRow").show();
    });
    $('#acceptAddFileButton').click(function (event) {
        event.preventDefault();
        let file = {};
        let row = $("#newRow").find('td')
            .not('td:nth-child(4)')
            .not('td:nth-child(5)');
        row.each(function (i) {
            if (i === 0) {
                file.name = $(this).find("input").val();
            }
            if (i === 1) {
                let raw = $(this).find("input").val();
                file.createdAt = moment(raw).format('DD-MM-YYYY HH:mm:ss');
            }
            if (i === 2) {
                let raw = $(this).find("input").val();
                file.updatedAt = moment(raw).format('DD-MM-YYYY HH:mm:ss');
            }
        })
        fetch(`http://vcs-server/v1/${projName}/apply`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
            },
            body: JSON.stringify({
                files: [file]
            })
        }).then((res) => {
            $("#newRow").hide();
            return res
        })
            .then(convertMiddleware)
            .then((body) => {
                datatable.row.add([file.name, moment(file.createdAt, 'DD-MM-YYYY HH:mm:ss').toISOString(),
                    moment(file.updatedAt, 'DD-MM-YYYY HH:mm:ss').toISOString()]).draw();
            })
            .then(() => {
                editFileButtonHandler(projName);
                deleteFileButtonHandler(projName);
            })
            .then((res) => renderInfo(`new file ${file.name} has been created`));
    })
}

let editProjectNameHandler = () => {
    $('#changeProjectNameButton').click(function (event) {
        event.preventDefault();
        let prevName = $('#projectNameSpan').text();
        $('#projectNameSpan').html("").append("<input type='text' value=\"" + prevName + "\">");

        event.target.outerHTML = '<button type="submit" id="applyProjectChangesButton">Применить</button>';

        $('#applyProjectChangesButton').click(function (event) {
            fetch(`http://vcs-server/v1/${prevName}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
                },
                body: JSON.stringify({
                    name: $('#projectNameSpan').find('input').val()
                })
            }).then((res) => {
                let newName = $('#projectNameSpan').find('input').val()
                $('#projectNameSpan').text(newName);
                window.localStorage.setItem('clickedProject', newName);
                event.target.outerHTML = '<button type="submit" id="changeProjectNameButton" value="Submit">ред.</button>';
            })
                .then(convertMiddleware)
                .then(() => renderInfo(`project name has been changed`))
                .then(() => editProjectNameHandler());
        })
    });
}

let editOwnersHandler = (projName) => {
    $('#changeOwnersButton').click(function (event) {
        event.preventDefault();
        let prevOwners = $('#projectOwnersSpan').text();
        $('#projectOwnersSpan').html("").append("<input type='text' value=\"" + prevOwners + "\">");

        event.target.outerHTML = '<button type="submit" id="applyOwnersChangesButton">Применить</button>';

        $('#applyOwnersChangesButton').click(function (event) {
            fetch(`http://vcs-server/v1/${projName}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
                },
                body: JSON.stringify({
                    owners: $('#projectOwnersSpan').find('input').val().replace(/ /g, '').split(',')
                })
            })
                .then(convertMiddleware)
                .then(() => {
                let newOwners = $('#projectOwnersSpan').find('input').val();
                $('#projectOwnersSpan').text(newOwners);
                event.target.outerHTML = '<button type="submit" id="changeOwnersButton" value="Submit">ред.</button>';
            })
                .then((res) => renderInfo(`project owners has been changed`))
                .then(() => editOwnersHandler(projName));
        })
    });
}