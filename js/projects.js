$(document).ready(() => {
    renderProjectsPage().then(r => removeProjectHandler());

    $('#projectsList').on('click', 'a', (event) => {
        event.preventDefault();
        window.localStorage.setItem('clickedProject', event.target.textContent);
        location.href = '../project';
    });
});

let renderProjectsPage = async () => {
    let body = fetch("http://vcs-server/v1/projects", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
        },
    }).then(convertMiddleware);
    let projects = (await body).projects;
    projects.map(proj => {
        $('#projectsList').append(() => {
                let el = document.createElement('li');
                el.innerHTML = `<a href="#"/>${proj.name}</a>  <button type="submit" class="removeProjectButton" value="Submit">Удалить</button> `;
                return el;
            }
        )
    });
}

let removeProjectHandler = () => {
    $('.removeProjectButton').click(async function () {
        let projName = $(this).closest('li').find('a').text();
        fetch(`http://vcs-server/v1/${projName}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
            },
        })
            .then(convertMiddleware)
            .then(() => {
                $(this).closest('li').remove();
            }).catch((err)=>{});
    })
}

let addProjectHandler = () => {
    $('.removeProjectButton').click(async function () {
        let projName = $(this).closest('li').find('a').text();
        fetch(`http://vcs-server/v1/${projName}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${window.sessionStorage.getItem('jwt')}`
            },
        })
            .then(convertMiddleware)
            .then(() => {
                $(this).closest('li').remove();
            }).catch((err)=>{});
    })
}